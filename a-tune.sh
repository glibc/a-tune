# A-Tune

#yum clean packages
yum install -y --nogpgcheck golang-bin python3 perf sysstat hwloc-gui python3-dict2xml python3-flask-restful python3-pandas python3-scikit-optimize python3-xgboost python3-pyyaml

cd ~
git clone https://gitee.com/openeuler/A-Tune.git

ip a

fdisk -l | grep dev

cp /root/A-Tune/misc/atuned.cnf{,.origin}
vim /root/A-Tune/misc/atuned.cnf
diff /root/A-Tune/misc/atuned.cnf{,.origin}

#yum install nano --nogpgcheck
#nano /root/A-Tune/misc/atuned.cnf

cd ~/A-Tune/
make
make collector-install
make install
make startup





